import random


def gen_start(len_grille):
    grille = []
    for x in range(len_grille):
        grille_tmp = []
        for y in range(len_grille):
            grille_tmp.append("-")
        grille.append(grille_tmp)
    x_robot,y_robot = gen_position_random(len_grille)
    grille[x_robot][y_robot] = "R"
    return grille,x_robot,y_robot


def print_2d(grille):
    for x in grille:
        line = ""
        for y in x:
            line += y + "  "
        print(line)


def gen_position_random(len_grille):
    x = random.randint(0, len_grille - 1)
    y = random.randint(0, len_grille - 1)
    return x, y


def gen_minerai(len_grille, grille):
    x_minerai, y_minerai = gen_position_random(len_grille)
    grille[x_minerai][y_minerai] = "M"
    return grille,x_minerai, y_minerai


def direction():
    saisie = ""
    while saisie not in ["d", "g", "h", "b"]:
        saisie = input(" Dans quel direction vous souhaitez aller ? d,g,h,b \n")
    return saisie

def deplacement_robot(grille, x_robot, y_robot, saisie, len_grille):
    grille[x_robot][y_robot] = "-"
    if saisie == "d":
        if y_robot == len_grille - 1:
            y_robot = 0
        else:
            y_robot = y_robot + 1
    elif saisie == "g":
        if y_robot == 0:
            y_robot = len_grille - 1
        else:
            y_robot = y_robot - 1
    elif saisie == "h":
        if x_robot == 0:
            x_robot = len_grille - 1
        else:
            x_robot = x_robot - 1

    elif saisie == "b":
        if x_robot == len_grille - 1:
            x_robot = 0
        else:
            x_robot = x_robot + 1
    grille[x_robot][y_robot] = "R"
    return grille, x_robot, y_robot

def check_robot_minerai(x_robot, y_robot):
    if [x_robot, y_robot] in position_minerai:
        print("tacam robot on minerai")

#GLOBAL VAR
position_minerai = []
len_grille = 20

if __name__ == '__main__':
    grille, x_robot, y_robot = gen_start(len_grille)
    print_2d(grille)
    count = 0
    while 1:
        if count % 4 == 0:
            grille, x_minerai, y_minerai = gen_minerai(len_grille, grille)
            position_minerai.append([x_minerai, y_minerai])
        check_robot_minerai(x_robot, y_robot)
        saisie = direction()
        grille, x_robot, y_robot = deplacement_robot(grille, x_robot, y_robot, saisie, len_grille)
        check_robot_minerai(x_robot, y_robot)
        print_2d(grille)
        count += 1



